# Namrats Haskell Templates

[Stack][haskellstack] templates for starting a Haskell project.

[![(c) William RM Bartlett, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

## License

See [LICENSE.md](LICENSE.md)

[haskellstack]: http://haskellstack.org/
